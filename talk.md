---
author: Shawn Nock
institute: Affiliations
title: Title
subtitle: Subtitle
lang: en-CA
...

# Section 1

## Slide 1

## Seven Segment Display

> * One
> * Two
> * Three

## The End?

\begin{center}
\LARGE{Questions?}
\end{center}
